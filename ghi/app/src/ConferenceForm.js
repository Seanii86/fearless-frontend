import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            maxAttendees: '',
            maxPresentations: '',
            locations: []
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartDate = this.handleStartDate.bind(this);
        this.handleEndDate = this.handleEndDate.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentationChange = this.handleMaxPresentationChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }



    // ======================================
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value});
    }
    handleStartDate(event) {
        const value = event.target.value;
        this.setState({starts: value});
    }
    handleEndDate(event) {
        const value = event.target.value;
        this.setState({ends: value});
    }
    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value});
    }
    handleMaxPresentationChange(event) {
        const value = event.target.value;
        this.setState({maxPresentations: value});
    }
    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({maxAttendees: value});
    }
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value});
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.max_presentations = data.maxPresentations
        data.max_attendees = data.maxAttendees
        delete data.maxPresentations;
        delete data.maxAttendees;
        delete data.locations;

        const url = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                maxAttendees: '',
                maxPresentations: '',
                location: 'location'
            }
            this.setState(cleared);
        }
    }
    // ======================================

    async componentDidMount(){
        const url = "http://localhost:8000/api/locations/";
        const response = await fetch(url);
    
        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
            
        }
    }



    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} placeholder="Name" value={this.state.name} required type="text" id="name" name="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleStartDate} placeholder="starts" value={this.state.starts} required type="date" id="starts" name="starts" className="form-control"/>
                    <label htmlFor="starts">Starts</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEndDate} placeholder="ends" value={this.state.ends} required type="date" id="ends" name="ends" className="form-control"/>
                    <label htmlFor="ends">Ends</label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="description" className="form-label">Description</label>
                    <textarea onChange={this.handleDescriptionChange} value={this.state.description} className="form-control" rows="3" id="description" name="description"></textarea>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleMaxPresentationChange} placeholder="max_presentations" value={this.state.maxPresentations} required type="number" id="max_presentations"
                      name="max_presentations" className="form-control"/>
                    <label htmlFor="max_presentations">Maximum presentations</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleMaxAttendeesChange} placeholder="max_attendees" value={this.state.maxAttendees} required type="number" id="max_attendees" name="max_attendees"
                      className="form-control"/>
                    <label htmlFor="max_attendees">Maximum attendees</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleLocationChange} value={this.state.location} required id="location" name="location" className="form-select">
                      <option value="">Choose a location</option>
                      {this.state.locations.map(location => {
                        return (
                            <option key={location.name} value={location.name}>
                                {location.name}
                            </option>
                        );
                      })};
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        )
    }
}
export default ConferenceForm;