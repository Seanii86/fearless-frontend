import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            conference: '',
            PresenterName: '',
            companyName: '',
            titleName: '',
            synopsis: '',
            PresenterEmail: '',
            conferences: [],
            hasSignedUp: false,
        }

        this.handlePresenterName = this.handlePresenterName.bind(this);
        this.handlePresenterEmail = this.handlePresenterEmail.bind(this);
        this.handleCompanyName = this.handleCompanyName.bind(this);
        this.handleTitleName = this.handleTitleName.bind(this);
        this.handleSynopsis = this.handleSynopsis.bind(this);
        this.handleConference = this.handleConference.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ conferences: data.conferences });
        }
    }



    // ====================Handelers========================
    handlePresenterName(event) {
        const value = event.target.value;
        this.setState({ PresenterName: value });
    }
    handlePresenterEmail(event) {
        const value = event.target.value;
        this.setState({ PresenterEmail: value });
    }
    handleCompanyName(event) {
        const value = event.target.value;
        this.setState({ companyName: value });
    }
    handleTitleName(event) {
        const value = event.target.value;
        this.setState({ titleName: value });
    }
    handleSynopsis(event) {
        const value = event.target.value;
        this.setState({ synopsis: value });
    }
    handleConference(event) {
        const value = event.target.value;
        this.setState({ conference: value });
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.presenter_name = data.PresenterName
        data.presenter_email = data.PresenterEmail
        data.company_name = data.companyName
        data.title = data.titleName
        delete data.titleName;
        delete data.PresenterEmail;
        delete data.companyName;
        delete data.PresenterName;
        delete data.conferences;
        delete data.hasSignedUp;
        console.log("::::", data)

        const selectTag = document.getElementById('conference');
        const conferenceId = selectTag.options[selectTag.selectedIndex].value; // "/api/conferences/6/"

        const url = `http://localhost:8000${conferenceId}presentations/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            console.log(data)
            this.setState({
                conference: '',
                PresenterName: '',
                companyName: '',
                titleName: '',
                synopsis: '',
                PresenterEmail: '',
            })
        }
    }
    // ====================Handelers========================



    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={this.handleSubmit} id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePresenterName} value={this.state.PresenterName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                                <label htmlFor="presenter_name">Presenter name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePresenterEmail} value={this.state.PresenterEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                                <label htmlFor="presenter_email">Presenter email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleCompanyName} value={this.state.companyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" />
                                <label htmlFor="company_name">Company name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleTitleName} value={this.state.titleName} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="synopsis">Synopsis</label>
                                <textarea onChange={this.handleSynopsis} value={this.state.synopsis} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleConference} value={this.state.conference} required name="conference" id="conference" className="form-select">
                                    <option value="">Choose a conference</option>
                                    {this.state.conferences.map(conference => {
                                        return (
                                            <option value={conference.href} key={conference.href}>
                                                {conference.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default PresentationForm;