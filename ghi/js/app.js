function createPlaceHolderCard() {
  return `
  <div class="card" aria-hidden="true">
  <img src="https://i.kym-cdn.com/photos/images/newsfeed/001/162/139/d1a.jpg" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title placeholder-glow">
      <span class="placeholder col-6"></span>
    </h5>
    <p class="card-text placeholder-glow">
      <span class="placeholder col-7"></span>
      <span class="placeholder col-4"></span>
      <span class="placeholder col-4"></span>
      <span class="placeholder col-6"></span>
      <span class="placeholder col-8"></span>
    </p>
    <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
  </div>
</div>
  `
}

function createCard(name, description, pictureUrl, starts, ends, location) {
  return `
    <div class="card">
      <div class="shadow">
        <img src=${pictureUrl} class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          <small class="text-muted">${starts} - ${ends}</small>
        </div>
      </div>
    </div>
  `
  
}


function alertComponent(){
  return `
  <div class="alert alert-danger d-flex align-items-center" role="alert">
    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
    <div>
      You messed up somewhere
    </div>
</div>
`
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
      const response = await fetch(url);
      if (!response.ok) {
        alert("response not okay")

      } else {
        const data = await response.json();
        let index = 0;

        for (let conference of data.conferences) {
          const placeholder = createPlaceHolderCard()
          const row = document.querySelector(`#col-${index % 3}`)
          row.innerHTML += placeholder
          index += 1;
        }


        for (let conference of data.conferences){
        const DetailUrl = `http://localhost:8000${conference.href}`;
        const DetailResponse = await fetch(DetailUrl);
        if (DetailResponse.ok) {
          const details = await DetailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts = new Date(details.conference.starts).toLocaleDateString()
          const ends = new Date(details.conference.ends).toLocaleDateString()
          const location = details.conference.location.name;
          const html = createCard(title, description, pictureUrl, starts, ends, location);
          const column = document.querySelector(`#col-${index % 3}`);
          column.innerHTML = column.innerHTML.replace(createPlaceHolderCard(), html);
          index += 1;
        }
      }
      }
    } catch (e) {
      console.error("got an error", e)
      const alertHTML = alertComponent()
      const alertMessage = document.querySelector('.alert-message')
      alertMessage.innerHTML = alertHTML
    }
  
  });
