// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get("jwt_access_payload");

if (payloadCookie) {
  const decodedPayload = atob(payloadCookie.value);
  const payload = JSON.parse(decodedPayload);
 

  // Check if "events.add_conference" is in the permissions.
  const perms = payload.user.perms;
  const locationNav = document.getElementById('loggedIn')
  const conferenceNav = document.getElementById('loggedIn2')
  const presentationNav = document.getElementById('loggedIn3')
  const loginNav = document.getElementById('loggedIn4')
  if (perms.includes('events.add_conference')) {
      // If it is, remove 'd-none' from the link
    locationNav.classList.remove('d-none')
    conferenceNav.classList.remove('d-none')
    presentationNav.classList.remove('d-none')
    loginNav.classList.add('d-none')
  }

  // Check if "events.add_location" is in the permissions.
  if (perms.includes('events.add_location')){
    locationNav.classList.remove('d-none')
    conferenceNav.classList.remove('d-none')
    presentationNav.classList.remove('d-none')
    loginNav.classList.add('d-none')
  }
  // If it is, remove 'd-none' from the link

}